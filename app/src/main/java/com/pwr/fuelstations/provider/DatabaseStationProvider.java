package com.pwr.fuelstations.provider;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.pwr.fuelstations.AppApplication;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;

import java8.util.stream.Collectors;
import java8.util.stream.StreamSupport;

/**
 * Created by cliffroot on 09.01.16.
 */
@EBean
public class DatabaseStationProvider implements StationProvider {

    Context context;

    public DatabaseStationProvider (Context context) {
        this.context = context;
    }

    @Override
    @Background
    public void getAllStations(Callback callback) {
        callback.onDone(((AppApplication) context.getApplicationContext()).stationDao.getAll());
    }

    @Override
    @Background
    public void getAllStationWithinBounds(LatLngBounds bounds, Callback callback) {

        callback.onDone(StreamSupport.stream(((AppApplication) context.getApplicationContext()).stationDao.getAll())
                .filter(stationPosition -> bounds.contains(new LatLng(stationPosition.getLatitude(), stationPosition.getLongitude()
        ))).collect(Collectors.toList()));
    }
}
