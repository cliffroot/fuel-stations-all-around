package com.pwr.fuelstations.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.pwr.fuelstations.AppApplication;
import com.pwr.fuelstations.model.StationPosition;
import com.pwr.fuelstations.provider.StationProvider;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by cliffroot on 21.11.15.
 */
public class Utils {

    private static final String FAVORITES_PREF_NAME = "favorites";
    private static final String LAST_UPDATED_NAME   = "last_updated";

    public static double kilometersBetween(LatLng from, LatLng to) {
        Location f = new Location("");
        f.setLatitude(from.latitude);
        f.setLongitude(from.longitude);

        Location t = new Location("");
        t.setLatitude(to.latitude);
        t.setLongitude(to.longitude);

        return f.distanceTo(t) / 1000.;
    }

    public static float roundToHalf(float x) {
        return (float) (Math.ceil(x * 2) / 2);
    }

    public static void writeToFavorites (Context context, StationPosition position) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String favorites = sp.getString(FAVORITES_PREF_NAME, "");
        Log.e("STATION", "" + position.getOn() + ", " + position.getCNG());
        favorites += (position.getId() + "#");
        sp.edit().putString(FAVORITES_PREF_NAME, favorites).commit();
    }

    public static boolean isInFavorites (Context context, StationPosition position) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String favorites = sp.getString(FAVORITES_PREF_NAME, "");
        return favorites.contains("#" + position.getId() + "#") || favorites.startsWith(position.getId() + "#");
    }

    public static void removeFromFavorites (Context context, StationPosition position) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String favorites = sp.getString(FAVORITES_PREF_NAME, "");
        favorites = favorites.replace("#" + position.getId() + "#", "");
        if (favorites.startsWith(position.getId() + "#")) {
            favorites = favorites.replace(position.getId() + "#", "");
        }
        sp.edit().putString(FAVORITES_PREF_NAME, favorites).commit();
    }

    public static void getFavoriteStations (Context context, StationProvider provider, StationProvider.Callback callback) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String favorites = sp.getString(FAVORITES_PREF_NAME, "");

        List<StationPosition> res = new LinkedList<>();
        String[] stations = favorites.split("#");

        for (String station: stations) {
            res.add(((AppApplication) context.getApplicationContext()).stationDao.getById(Long.valueOf(station)));
        }
        callback.onDone(res);
    }

    public static void setLastUpdated (Context context, long timeStamp) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        sp.edit().putLong(LAST_UPDATED_NAME, timeStamp).commit();
    }

    public static long getLastUpdated (Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        return sp.getLong(LAST_UPDATED_NAME, 0);
    }

    public static List<StationPosition> limitResult (List<StationPosition> stations, int limit) {
        List<StationPosition> result = new ArrayList<>();

        Random random = new Random();
        for (int i = 0; i < limit; i++) {
            StationPosition randomStation = stations.get(random.nextInt(stations.size()));
            result.add(randomStation);
            stations.remove(randomStation);
        }

        return result;
    }

}
