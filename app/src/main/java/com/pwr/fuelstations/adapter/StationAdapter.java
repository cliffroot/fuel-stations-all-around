package com.pwr.fuelstations.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.pwr.fuelstations.R;
import com.pwr.fuelstations.StationActivity;
import com.pwr.fuelstations.StationActivity_;
import com.pwr.fuelstations.helper.Utils;
import com.pwr.fuelstations.model.StationPosition;
import com.pwr.fuelstations.provider.GoogleApiPathProvider_;
import com.pwr.fuelstations.provider.PathProvider;

import java.util.List;

/**
 * Created by cliffroot on 21.11.15.
 */
public class StationAdapter extends RecyclerView.Adapter<StationAdapter.StationCardHolder> {


    private Context context;
    private List<StationPosition> stations;
    private LatLng currentLocation;
    private int fuelType;

    private boolean showPrice;

    PathProvider pathProvider;

    public StationAdapter (Context context, List<StationPosition> stations, LatLng currentLocation, int currentFuelType, boolean showPrice) {
        this.context = context;
        this.stations = stations;
        this.currentLocation = currentLocation;
        this.showPrice = showPrice;
        fuelType = currentFuelType;
        pathProvider = GoogleApiPathProvider_.getInstance_(context);

    }

    @Override
    public StationCardHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.card_station_list, parent, false);
        if (!showPrice) {
            view.findViewById(R.id.station_list_price).setVisibility(View.INVISIBLE);
        }
        return new StationCardHolder(view);
    }



    @Override
    public void onBindViewHolder(final StationCardHolder holder,final int position) {
        final StationPosition sp = stations.get(position);

        if (sp.getName().startsWith(" ")) {
            sp.setName(sp.getName().substring(1));
        }
        holder.stationNameView.setText(sp.getName());
        String address = sp.getAddress();
        while (address.contains("  ")) {
            address = address.replace("  " , "");
        }
        if (address.endsWith(" ")) {
            address = address.substring(0, address.length() - 1);
        }
        if (sp.getDistanceFromMe() != null) {
            holder.stationNameAddressView.setText(Html.fromHtml(address + ", <i>" + Utils.roundToHalf(sp.getDistanceFromMe()) + " km</i>"));
        } else {
            sp.setDistanceFromMe((float) Utils.kilometersBetween(currentLocation, new LatLng(sp.getLatitude(), sp.getLongitude())));
            holder.stationNameAddressView.setText(Html.fromHtml(address + ", <i>" + Utils.roundToHalf(sp.getDistanceFromMe()) + " km</i>"));
        }
        String price = StationPosition.functions.get(fuelType).apply(stations.get(position));
        while (price.contains("  ")) {
            price = price.replace("  ", "");
        }
        holder.priceView.setText(price + "zł/dm3");

        holder.itemView.setOnClickListener(view -> {
            Intent intent = new Intent(context, StationActivity_.class);
            intent.putExtra(StationActivity.STATION_EXTRA_ARG, sp);
            intent.putExtra(StationActivity.CURRENT_POSITION_ARG, currentLocation);
            context.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return stations.size();
    }

    public static class StationCardHolder extends RecyclerView.ViewHolder {

        TextView stationNameView;
        TextView stationNameAddressView;
        TextView distanceToView;
        TextView priceView;

        public StationCardHolder (View view) {
            super(view);
            stationNameView = ((TextView) view.findViewById(R.id.station_list_name));
            stationNameAddressView = ((TextView) view.findViewById(R.id.station_list_address));
            distanceToView = ((TextView) view.findViewById(R.id.distance_to));
            priceView = ((TextView) view.findViewById(R.id.station_list_price));
        }
    }

}
