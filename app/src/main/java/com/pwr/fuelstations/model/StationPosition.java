package com.pwr.fuelstations.model;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.pwr.fuelstations.database.DatabaseColumns;

import java.util.LinkedList;
import java.util.List;

import java8.util.function.Function;

/**
 * Created by cliffroot on 21.11.15.
 */

public class StationPosition implements Parcelable{

    @SerializedName("owner_name")
    String name;

    @SerializedName("address")
    String address;

    @SerializedName("price_pb95")
    String pb95;

    @SerializedName("price_pb98")
    String pb98;

    @SerializedName("price_on")
    String on;

    @SerializedName("price_on_bio")
    String onbio;

    @SerializedName("price_lpg")
    String LPG;

    @SerializedName("price_cng")
    String CNG;

    @SerializedName("gas_station_lat")
    double latitude;

    @SerializedName("gas_station_lng")
    double longitude;

    @SerializedName("id")
    long id;

    Float distanceFromMe;

    public static List<Function<StationPosition, String>> functions = new LinkedList<>();

    static {
        Function<StationPosition, String> f = StationPosition::getPb95; functions.add(f);
        f = StationPosition::getPb98; functions.add(f);
        f = StationPosition::getOn; functions.add(f);
        f = StationPosition::getOnbio; functions.add(f);
        f = StationPosition::getLPG; functions.add(f);
        f = StationPosition::getCNG; functions.add(f);
    }

    public long getId() {
        return id;
    }

    public StationPosition setId(long id) {
        this.id = id;
        return this;
    }

    public Float getDistanceFromMe() {
        return distanceFromMe;
    }

    public StationPosition setDistanceFromMe(float distanceFromMe) {
        this.distanceFromMe = distanceFromMe;
        return this;
    }

    public String getName() {
        return name;
    }

    public StationPosition setName(String name) {
        this.name = name;
        return this;
    }

    public String getPb95() {
        return pb95;
    }

    public StationPosition setPb95(String pb95) {
        this.pb95 = pb95;
        return this;
    }

    public String getPb98() {
        return pb98;
    }

    public StationPosition setPb98(String pb98) {
        this.pb98 = pb98;
        return this;
    }

    public String getOn() {
        return on;
    }

    public StationPosition setOn(String on) {
        this.on = on;
        return this;
    }

    public String getOnbio() {
        return onbio;
    }

    public StationPosition setOnbio(String onbio) {
        this.onbio = onbio;
        return this;
    }

    public String getLPG() {
        return LPG;
    }

    public StationPosition setLPG(String LPG) {
        this.LPG = LPG;
        return this;
    }

    public String getCNG() {
        return CNG;
    }

    public StationPosition setCNG(String CNG) {
        this.CNG = CNG;
        return this;
    }

    public double getLatitude() {
        return latitude;
    }

    public StationPosition setLatitude(double latitude) {
        this.latitude = latitude;
        return this;
    }

    public double getLongitude() {
        return longitude;
    }

    public StationPosition setLongitude(double longitude) {
        this.longitude = longitude;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public StationPosition setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public String toString() {
        return "StationPosition{" +
                "name='" + name + '\'' +
                ", pb95='" + pb95 + '\'' +
                ", pb98='" + pb98 + '\'' +
                ", on='" + on + '\'' +
                ", onbio='" + onbio + '\'' +
                ", LPG='" + LPG + '\'' +
                ", CNG='" + CNG + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StationPosition that = (StationPosition) o;

        if (Double.compare(that.latitude, latitude) != 0) return false;
        if (Double.compare(that.longitude, longitude) != 0) return false;
        return !(name != null ? !name.equals(that.name) : that.name != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = name != null ? name.hashCode() : 0;
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.address);
        dest.writeString(this.pb95);
        dest.writeString(this.pb98);
        dest.writeString(this.on);
        dest.writeString(this.onbio);
        dest.writeString(this.LPG);
        dest.writeString(this.CNG);
        dest.writeDouble(this.latitude);
        dest.writeDouble(this.longitude);
        dest.writeLong(this.id);
    }

    public StationPosition() {

    }

    public StationPosition(String name, String address, String pb95, String pb98, String on,
                           String onbio, String LPG, String CNG, double latitude, double longitude, Float distanceFromMe, long id) {
        this.name = name;
        this.address = address;
        this.pb95 = pb95;
        this.pb98 = pb98;
        this.on = on;
        this.onbio = onbio;
        this.LPG = LPG;
        this.CNG = CNG;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distanceFromMe = distanceFromMe;
        this.id=id;
    }

    private StationPosition(Parcel in) {
        this.name = in.readString();
        this.address = in.readString();
        this.pb95 = in.readString();
        this.pb98 = in.readString();
        this.on = in.readString();
        this.onbio = in.readString();
        this.LPG = in.readString();
        this.CNG = in.readString();
        this.latitude = in.readDouble();
        this.longitude = in.readDouble();
        this.id = in.readLong();
    }

    public static final Parcelable.Creator<StationPosition> CREATOR = new Parcelable.Creator<StationPosition>() {
        public StationPosition createFromParcel(Parcel source) {
            return new StationPosition(source);
        }

        public StationPosition[] newArray(int size) {
            return new StationPosition[size];
        }
    };

    public ContentValues toContentValues () {
        ContentValues values = new ContentValues();

        values.put(DatabaseColumns.STATION_ADDRESS, address);
        values.put(DatabaseColumns.STATION_CNG, CNG);
        values.put(DatabaseColumns.STATION_LATITUDE, latitude);
        values.put(DatabaseColumns.STATION_LONGITUDE, longitude);
        values.put(DatabaseColumns.STATION_LPG, LPG);
        values.put(DatabaseColumns.STATION_NAME, name);
        values.put(DatabaseColumns.STATION_ON, on);
        values.put(DatabaseColumns.STATION_ONBIO, onbio);
        values.put(DatabaseColumns.STATION_PB95, pb95);
        values.put(DatabaseColumns.STATION_PB98, pb98);
        values.put(DatabaseColumns.STATION_ID, id);

        return values;
    }
}
