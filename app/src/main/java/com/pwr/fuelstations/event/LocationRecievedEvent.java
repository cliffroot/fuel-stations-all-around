package com.pwr.fuelstations.event;

import android.location.Location;

/**
 * Created by cliffroot on 21.11.15.
 */
public class LocationRecievedEvent {

    public Location location;

    public LocationRecievedEvent(Location location) {
        this.location = location;
    }
}
