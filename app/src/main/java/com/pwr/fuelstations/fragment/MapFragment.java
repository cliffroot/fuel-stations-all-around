package com.pwr.fuelstations.fragment;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.pwr.fuelstations.AppApplication;
import com.pwr.fuelstations.R;
import com.pwr.fuelstations.StationActivity;
import com.pwr.fuelstations.StationActivity_;
import com.pwr.fuelstations.event.LocationRecievedEvent;
import com.pwr.fuelstations.helper.SwipeDismissTouchListener;
import com.pwr.fuelstations.helper.Utils;
import com.pwr.fuelstations.model.StationPosition;
import com.pwr.fuelstations.provider.DatabaseStationProvider;
import com.pwr.fuelstations.provider.GoogleApiPathProvider;
import com.pwr.fuelstations.provider.PathProvider;
import com.pwr.fuelstations.provider.StationProvider;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import java8.util.stream.StreamSupport;

/**
 * Created by cliffroot on 21.11.15.
 */
@EFragment(R.layout.fragment_map)
public class MapFragment extends Fragment {

    static Fragment instance;

    @ViewById(R.id.map_view)
    MapView mapView;

    @ViewById(R.id.station_address)
    TextView stationAddressView;

    @ViewById(R.id.station_name)
    TextView stationNameView;

    @ViewById(R.id.station_card)
    CardView stationCard;

    @ViewById(R.id.see_more_button)
    Button seeMoreButton;

    @InstanceState
    LatLng lastLocation;

    @InstanceState
    float zoom;

    @InstanceState
    LatLng currentMarkerLocation;

    Map<Marker,StationPosition> markersToStationsMap = new ConcurrentHashMap<>();
    Map<StationPosition,Marker> stationsToMarkersMap = new ConcurrentHashMap<>();
    Marker currentMarker;

    @Bean(DatabaseStationProvider.class)
    StationProvider stationProvider;

    @Bean(GoogleApiPathProvider.class)
    PathProvider pathProvider;

    @InstanceState
    boolean animationFinished = false;

    public static Fragment getInstance () {
        if (instance == null) {
            instance = new MapFragment_();
        }
        return instance;
    }

    @Override
    public void onStart () {
        super.onStart();
        Bus bus = ((AppApplication) getActivity().getApplication()).bus;
        bus.register(this);


    }

    @AfterViews
    public void initializeMap () {
        mapView.onCreate(null);
    }

    @AfterViews
    public void setupDismissalListener () {
        stationCard.setOnTouchListener(new SwipeDismissTouchListener(stationCard, null, new SwipeDismissTouchListener.DismissCallbacks() {
            @Override
            public boolean canDismiss(Object token) {
                return true;
            }

            @Override
            public void onDismiss(View view, Object token) {
                view.setVisibility(View.GONE);
                currentMarker.hideInfoWindow();
                currentMarker = null;
                currentMarkerLocation = null;
            }
        }));
    }

    @Subscribe
    public void updateLocation (LocationRecievedEvent event) {
        if (lastLocation == null) {

            Log.e("LOCATION_RECEIVE", event.location + "");
            mapView.getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(event.location.getLatitude(),
                    event.location.getLongitude()),
                    event.location.getProvider().equals("Poland")? 5.f : 12.f), new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    addItems();
                    animationFinished = true;

                    mapView.getMap().setOnCameraChangeListener(cameraPosition -> {
                        lastLocation = cameraPosition.target;
                        zoom = cameraPosition.zoom;
                        if (System.currentTimeMillis() - previousInvocation > 1000) {
                            previousInvocation = System.currentTimeMillis();
                            if (currentMarker == null) {
                                addItems();
                            }
                        }
                    });
                }

                @Override
                public void onCancel() {

                }
            });
        }
    }

    @Override
    public void onResume () {
        super.onResume();
        mapView.onResume();
        populateMap();
    }

    long previousInvocation = 0;

    void addItems () {
        stationProvider.getAllStations(stations -> {
                if (getActivity() != null)
                getActivity().runOnUiThread(() ->
                        stationProvider.getAllStationWithinBounds(mapView.getMap().getProjection().getVisibleRegion().latLngBounds, stations1 -> {
                            if (getActivity() != null)
                            getActivity().runOnUiThread(() -> {

                                if (stations1.size() == 0) {
                                    return;
                                }

                                List<Marker> markersToRemove = new LinkedList<Marker>();
                                for (StationPosition position: stationsToMarkersMap.keySet()) {
                                        Marker m = stationsToMarkersMap.get(position);
                                        if (m == null) continue; // TODO: check how it's can be null in the first place
                                        markersToStationsMap.remove(m);
                                        stationsToMarkersMap.remove(position);
                                        markersToRemove.add(m);
                                }

                                for (StationPosition station : Utils.limitResult(stations1, 100 > stations1.size() ? stations1.size() : 100)) {
                                    if (stationsToMarkersMap.get(station) != null) {
                                        continue;
                                    }
                                    Marker marker = mapView.getMap().addMarker(new MarkerOptions().position(new LatLng(station.getLatitude(),
                                            station.getLongitude())).title(station.getName() + ", " + station.getAddress()));
                                    markersToStationsMap.put(marker, station);
                                    stationsToMarkersMap.put(station, marker);
                                    if (marker.getPosition().equals(currentMarkerLocation)) {
                                        clickMarker(marker);
                                    }
                                }

                                StreamSupport.stream(markersToRemove).forEach(Marker::remove);

                            });
                        }));});
    }

    public void populateMap () {
        final GoogleMap map = mapView.getMap();
        if (lastLocation != null) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(lastLocation, zoom));
        }

        map.setOnMapClickListener(latLng -> {
            stationCard.setVisibility(View.GONE);
            currentMarker = null;
            currentMarkerLocation = null;
        });

        if (animationFinished) {
            addItems();

            mapView.getMap().setOnCameraChangeListener(cameraPosition -> {
                lastLocation = cameraPosition.target;
                zoom = cameraPosition.zoom;
                if (System.currentTimeMillis() - previousInvocation > 1000) {
                    previousInvocation = System.currentTimeMillis();
                    addItems();
                }
            });
        }

        map.setOnMarkerClickListener(this::clickMarker);
    }

    public boolean clickMarker (Marker marker) {
        currentMarker = marker;
        currentMarkerLocation = marker.getPosition();
        stationCard.setVisibility(View.VISIBLE);
        final StationPosition sp = markersToStationsMap.get(marker);
        while (sp.getName().startsWith(" ")) {
            sp.setName(sp.getName().replace(" ", ""));
        }
        stationAddressView.setText(sp.getAddress());
        stationNameView.setText(sp.getName());
        seeMoreButton.setOnClickListener(view -> {
            Intent intent = new Intent(MapFragment.this.getActivity(), StationActivity_.class);
            intent.putExtra(StationActivity.STATION_EXTRA_ARG, sp);
            intent.putExtra(StationActivity.CURRENT_POSITION_ARG, ((AppApplication) getActivity().getApplication()).getCurrentLocation());
            startActivity(intent);
        });


        return false;
    }

    @Override
    public void onPause () {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onStop () {
        super.onStop();
        Bus bus = ((AppApplication) getActivity().getApplication()).bus;
        bus.unregister(this);
    }
}
