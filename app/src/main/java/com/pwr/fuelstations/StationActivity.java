package com.pwr.fuelstations;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.pwr.fuelstations.helper.Utils;
import com.pwr.fuelstations.model.StationPosition;
import com.pwr.fuelstations.provider.GoogleApiPathProvider;
import com.pwr.fuelstations.provider.PathProvider;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import java8.util.stream.StreamSupport;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


@EActivity(R.layout.activity_station)
@OptionsMenu(R.menu.station)
public class StationActivity extends AppCompatActivity {

    public static final String STATION_EXTRA_ARG = "stationExtraArg";
    public static final String CURRENT_POSITION_ARG = "currentPositionArg";

    @Extra(STATION_EXTRA_ARG)
    StationPosition station;

    @Extra(CURRENT_POSITION_ARG)
    LatLng currentLocation;

    @ViewById(R.id.map_view)
    MapView mapView;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById(R.id.station_pb95)
    TextView stationPb95View;

    @ViewById(R.id.station_pb98)
    TextView stationPb98View;

    @ViewById(R.id.station_on)
    TextView stationOnView;

    @ViewById(R.id.station_onbio)
    TextView stationOnBioView;

    @ViewById(R.id.station_lpg)
    TextView stationLpgView;

    @ViewById(R.id.station_cng)
    TextView stationCngView;

    @ViewById(R.id.distance_to_station)
    TextView distanceView;

    @ViewById(R.id.favorite_station_button)
    FloatingActionButton favoriteFab;

    @Bean(GoogleApiPathProvider.class)
    PathProvider pathProvider;

    GoogleMap map;

    @AfterViews
    protected void initializeUi () {
        setSupportActionBar(toolbar);
        //toolbar.setTitle(station.getName());
        getSupportActionBar().setTitle(station.getName());

        mapView.onCreate(new Bundle());
        map = mapView.getMap();
        map.setTrafficEnabled(true);
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 11.f));
        map.addMarker(new MarkerOptions().position(currentLocation).title("Me"));
        map.addMarker(new MarkerOptions().position(new LatLng(station.getLatitude(), station.getLongitude())).title(station.getName() + ", " + station.getAddress()));

        setPrices();

        pathProvider.getDistance(currentLocation, new LatLng(station.getLatitude(), station.getLongitude()), new PathProvider.Callback<Float>() {
            @Override
            public void onCompleted(Float result) {
                runOnUiThread(() -> distanceView.setText(Utils.roundToHalf(result) + " km"));
            }

            @Override
            public void onFail() {

            }
        });
    }

    @AfterViews
    protected void setupFab () {
        favoriteFab.setImageDrawable(Utils.isInFavorites(this, station) ? getResources().getDrawable(R.mipmap.ic_star_white_48dp)
                : getResources().getDrawable(R.mipmap.ic_star_border_white_48dp));
        favoriteFab.setOnClickListener((view) -> {
            if (!Utils.isInFavorites(this, station)) {
                Utils.writeToFavorites(this, station);
                favoriteFab.setImageDrawable(getResources().getDrawable(R.mipmap.ic_star_white_48dp));
                Snackbar.make(favoriteFab, "Added successfully", Snackbar.LENGTH_SHORT).setAction("UNDO", (v) -> {
                    Utils.removeFromFavorites(this, station);
                    favoriteFab.setImageDrawable(getResources().getDrawable(R.mipmap.ic_star_border_white_48dp));
                }).show();
            } else {
                Utils.removeFromFavorites(this, station);
                favoriteFab.setImageDrawable(getResources().getDrawable(R.mipmap.ic_star_border_white_48dp));
                Snackbar.make(favoriteFab, "Removed successfully", Snackbar.LENGTH_SHORT).setAction("UNDO", (v) -> {
                    Utils.writeToFavorites(this, station);
                    favoriteFab.setImageDrawable(getResources().getDrawable(R.mipmap.ic_star_white_48dp));
                }).show();
            }
        });
    }

    @SuppressLint("Assert")
    private void setPrices() {
        TextView[] views = new TextView[] {stationPb95View, stationPb98View, stationOnView, stationOnBioView, stationLpgView, stationCngView};
        assert(views.length == StationPosition.functions.size());
        for (int i = 0; i < views.length; i++) {
            String value = StationPosition.functions.get(i).apply(station);
            while(value.endsWith(" ")) {
                value = value.substring(0, value.length() - 1);
            }
            if (value.startsWith("-1.0")) {
                value = "N\\A";
            } else {
                value += "zł/dm3";
            }
            views[i].setText((value.isEmpty()?"N\\A                ":value));
        }

    }

    @Override
    public void onResume () {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }

        showPath();
    }

    void showPath () {
        pathProvider.getSegments(currentLocation, new LatLng(station.getLatitude(), station.getLongitude()), new PathProvider.Callback<List<LatLng>>() {
            @Override
            public void onCompleted(List<LatLng> result) {
                final PolylineOptions polyline = new PolylineOptions().color(Color.BLUE).width(5.f);
                StreamSupport.stream(result).forEach(polyline::add);
                runOnUiThread(() -> map.addPolyline(polyline));
            }

            @Override
            public void onFail() {

            }
        });
    }

    @OptionsItem(R.id.update_prices)
    void updatePrices () {
        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_update);

        Spinner fuelSpinner = (Spinner) dialog.findViewById(R.id.fuel_pick);
        EditText updatedPriceView = (EditText) dialog.findViewById(R.id.updated_price);
        View okButton = dialog.findViewById(R.id.ok_action);
        View cancelButton = dialog.findViewById(R.id.cancel_action);

        dialog.setTitle("Update prices");
        dialog.show();

        okButton.setOnClickListener((view) -> {
            try {
                Double.valueOf(updatedPriceView.getText().toString());
                switch (fuelSpinner.getSelectedItemPosition()) {
                    case 0:
                        stationPb95View.setText(updatedPriceView.getText() + "zł/dm3");
                        station.setPb95(updatedPriceView.getText().toString());
                        ((AppApplication) getApplication()).restService.updatePrice(station.getId(),
                                Double.valueOf(updatedPriceView.getText().toString()),
                                Double.valueOf(station.getPb98()),
                                Double.valueOf(station.getOn()),
                                Double.valueOf(station.getOnbio()),
                                Double.valueOf(station.getCNG()),
                                Double.valueOf(station.getLPG()), "",
                                new Callback<Void>() {
                                    @Override
                                    public void success(Void aVoid, Response response) {
                                        Log.e("UPDATE", "sucessfull");
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Log.e("UPDATE", "NOPE sucessfull", error);
                                    }
                                });
                        break;
                    case 1:
                        stationPb98View.setText(updatedPriceView.getText() + "zł/dm3");
                        station.setPb98(updatedPriceView.getText().toString());
                        ((AppApplication) getApplication()).restService.updatePrice(station.getId(),
                                Double.valueOf(station.getPb95()),
                                Double.valueOf(updatedPriceView.getText().toString()),
                                Double.valueOf(station.getOn()),
                                Double.valueOf(station.getOnbio()),
                                Double.valueOf(station.getCNG()),
                                Double.valueOf(station.getLPG()), "",
                                new Callback<Void>() {
                                    @Override
                                    public void success(Void aVoid, Response response) {
                                        Log.e("UPDATE", "sucessfull");
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Log.e("UPDATE", "NOPE sucessfull", error);
                                    }
                                });
                        break;
                    case 2:
                        stationOnView.setText(updatedPriceView.getText() + "zł/dm3");
                        station.setOn(updatedPriceView.getText().toString());
                        ((AppApplication) getApplication()).restService.updatePrice(station.getId(),
                                Double.valueOf(station.getPb95()),
                                Double.valueOf(station.getPb98()),
                                Double.valueOf(updatedPriceView.getText().toString()),
                                Double.valueOf(station.getOnbio()),
                                Double.valueOf(station.getCNG()),
                                Double.valueOf(station.getLPG()), "",
                                new Callback<Void>() {
                                    @Override
                                    public void success(Void aVoid, Response response) {
                                        Log.e("UPDATE", "sucessfull");
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Log.e("UPDATE", "NOPE sucessfull", error);
                                    }
                                });
                        break;
                    case 3:
                        stationOnBioView.setText(updatedPriceView.getText() + "zł/dm3");
                        station.setOnbio(updatedPriceView.getText().toString());
                        ((AppApplication) getApplication()).restService.updatePrice(station.getId(),
                                Double.valueOf(station.getPb95()),
                                Double.valueOf(station.getPb98()),
                                Double.valueOf(station.getOn()),
                                Double.valueOf(updatedPriceView.getText().toString()),
                                Double.valueOf(station.getCNG()),
                                Double.valueOf(station.getLPG()), "",
                                new Callback<Void>() {
                                    @Override
                                    public void success(Void aVoid, Response response) {
                                        Log.e("UPDATE", "sucessfull");
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Log.e("UPDATE", "NOPE sucessfull", error);
                                    }
                                });
                        break;
                    case 4:
                        stationCngView.setText(updatedPriceView.getText() + "zł/dm3");
                        station.setCNG(updatedPriceView.getText().toString());
                        ((AppApplication) getApplication()).restService.updatePrice(station.getId(),
                                Double.valueOf(station.getPb95()),
                                Double.valueOf(station.getPb98()),
                                Double.valueOf(station.getOn()),
                                Double.valueOf(station.getOnbio()),
                                Double.valueOf(updatedPriceView.getText().toString()),
                                Double.valueOf(station.getLPG()), "",
                                new Callback<Void>() {
                                    @Override
                                    public void success(Void aVoid, Response response) {
                                        Log.e("UPDATE", "sucessfull");
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Log.e("UPDATE", "NOPE sucessfull", error);
                                    }
                                });
                        break;
                    case 5:
                        stationLpgView.setText(updatedPriceView.getText() + "zł/dm3");
                        station.setLPG(updatedPriceView.getText().toString());
                        ((AppApplication) getApplication()).restService.updatePrice(station.getId(),
                                Double.valueOf(station.getPb95()),
                                Double.valueOf(station.getPb98()),
                                Double.valueOf(station.getOn()),
                                Double.valueOf(station.getOnbio()),
                                Double.valueOf(station.getCNG()),
                                Double.valueOf(updatedPriceView.getText().toString()), "",
                                new Callback<Void>() {
                                    @Override
                                    public void success(Void aVoid, Response response) {
                                        Log.e("UPDATE", "sucessfull");
                                    }

                                    @Override
                                    public void failure(RetrofitError error) {
                                        Log.e("UPDATE", "NOPE sucessfull", error);
                                    }
                                });
                        break;
                }
                ((AppApplication) getApplication()).stationDao.update(station);
                dialog.dismiss();
                Toast.makeText(this, "Sucessfully updated", Toast.LENGTH_LONG).show();
            } catch (Exception ex) {
                Toast.makeText(this, "Wrong value of price", Toast.LENGTH_LONG).show();
                dialog.dismiss();
            }
        });

        cancelButton.setOnClickListener((view) -> {
            dialog.dismiss();
        });

    }


}
