package com.pwr.fuelstations.provider;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLngBounds;
import com.pwr.fuelstations.AppApplication;
import com.pwr.fuelstations.model.StationPosition;
import com.pwr.fuelstations.rest.RestService;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;

import java.util.List;

import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by cliffroot on 18.01.16.
 */
@EBean
public class RestStationProvider implements StationProvider {

    Context context;

    public RestStationProvider (Context context) {
        this.context = context;
    }

    @Override
    @Background
    public void getAllStations(Callback callback) {
        RestService service = ((AppApplication) context.getApplicationContext()).restService;

        service.retrieveStations(new retrofit.Callback<List<StationPosition>>() {
            @Override
            public void success(List<StationPosition> stationPositions, Response response) {
                //StreamSupport.stream(stationPositions).forEach(((AppApplication) context.getApplicationContext()).stationDao::insert);
                callback.onDone(stationPositions);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.e("RECEIVE", "nope", error);
            }
        });

    }

    @Override
    public void getAllStationWithinBounds(LatLngBounds bounds, Callback callback) {
        //DUMMY IMPLEMENTATION
        callback.onDone(((AppApplication) context.getApplicationContext()).stationDao.getAll().subList(0, 100));
    }
}
