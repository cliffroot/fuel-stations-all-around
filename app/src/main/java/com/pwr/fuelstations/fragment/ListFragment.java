package com.pwr.fuelstations.fragment;

import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.pwr.fuelstations.R;
import com.pwr.fuelstations.adapter.StationAdapter;
import com.pwr.fuelstations.helper.Utils;
import com.pwr.fuelstations.model.StationPosition;
import com.pwr.fuelstations.provider.DatabaseStationProvider;
import com.pwr.fuelstations.provider.StationProvider;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by cliffroot on 21.11.15.
 */
@EFragment(R.layout.fragment_list)
public class ListFragment extends Fragment {

    public final static String CURRENT_LOCATION_ARG = "current_location_arg";

    @ViewById(R.id.recycler_view_stations)
    RecyclerView recyclerViewStations;

    @ViewById(R.id.fuel_pick)
    Spinner fuelPicker;

    @ViewById(R.id.radius_pick)
    SeekBar radiusPicker;

    @ViewById(R.id.radius_pick_text)
    TextView radiusPickerText;

    @Bean(DatabaseStationProvider.class)
    StationProvider stationProvider;

    @FragmentArg(CURRENT_LOCATION_ARG)
    LatLng currentLocation;

    @ViewById(R.id.station_loading_progress_bar)
    ProgressBar stationLoadingProgressBar;

    @ViewById(R.id.empty_recycler_view)
    TextView emptyRecyclerView;

    @InstanceState
    int currentRadius = 5;

    @InstanceState
    int currentFuelType = 0;

    static Fragment instance;

    public static Fragment getInstance (LatLng currentLocation) {
        if (instance == null) {
            instance = new ListFragment_();
            Bundle bundle = new Bundle();
            bundle.putParcelable(CURRENT_LOCATION_ARG, currentLocation);
            instance.setArguments(bundle);
        }
        return instance;
    }

    @AfterViews
    void setupSpinner () {
        fuelPicker.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                currentFuelType = i;
                startLoading();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    @AfterViews
    void setupRadiusPicker () {
        radiusPicker.incrementProgressBy(10);
        radiusPicker.setMax(9);
        radiusPicker.setProgress(5);

        radiusPickerText.setText("Radius: 5 km. ");
        radiusPicker.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                radiusPickerText.setText("Radius: " + (i + 1) + " km.");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                currentRadius = seekBar.getProgress() + 1;
                startLoading();
            }
        });
    }

    @UiThread
    void startLoading() {
        stationLoadingProgressBar.setVisibility(View.VISIBLE);
        recyclerViewStations.setVisibility(View.INVISIBLE);

        performSorting();
    }



    @AfterViews
    void setupRecyclerView () {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerViewStations.setLayoutManager(layoutManager);
        recyclerViewStations.setItemAnimator(new DefaultItemAnimator());
        performSorting();
    }

    @Background
    void performSorting () {
        stationProvider.getAllStations(stations -> {

            List <StationPosition> result = new LinkedList<>();

            for (int i = 0; i < stations.size(); i++) {
                StationPosition station = stations.get(i);
                double d;
                if (station != null && !TextUtils.isEmpty(StationPosition.functions.get(currentFuelType).apply(station).replaceAll("\\s", ""))
                        && Double.valueOf(StationPosition.functions.get(currentFuelType).apply(station)) != -1.0d
                        && (d = Utils.kilometersBetween(currentLocation, new LatLng(station.getLatitude(), station.getLongitude()))) <= currentRadius) {
                    station.setDistanceFromMe((float) d);
                    result.add(station);
                }
            }

            Collections.sort(result, (t0, t1) ->
                    StationPosition.functions.get(currentFuelType).apply(t0).compareTo(StationPosition.functions.get(currentFuelType).apply(t1)));
//            StreamSupport.stream(stations)
//                    .filter(station ->
//                            (station != null && !TextUtils.isEmpty(StationPosition.functions.get(currentFuelType).apply(station).replaceAll("\\s", ""))
//                                    && (Utils.kilometersBetween(currentLocation, new LatLng(station.getLatitude(), station.getLongitude()))) <= currentRadius)) // filter out the ones which are far away
//                    .map((station) ->
//                            station.setDistanceFromMe((float) (Utils.kilometersBetween(currentLocation,
//                                    new LatLng(station.getLatitude(), station.getLongitude()))))) // set the distance for each one
//                    .sorted((t0, t1) -> StationPosition.functions.get(currentFuelType).apply(t0).compareTo(StationPosition.functions.get(currentFuelType).apply(t1))) // sort them
//                    .collect(Collectors.toList()); // get the list
            if (getActivity() == null) return;
            getActivity().runOnUiThread(() -> publishResults(result));
        });
    }

    void publishResults (List<StationPosition> stations) {
        StationAdapter adapter = new StationAdapter(getActivity(), stations, currentLocation, currentFuelType, true);
        if (getActivity() != null)
        getActivity().runOnUiThread(() -> {
            recyclerViewStations.setAdapter(adapter);
            recyclerViewStations.setVisibility(View.VISIBLE);
            stationLoadingProgressBar.setVisibility(View.INVISIBLE);
        });
        adapter.notifyDataSetChanged();

        if (adapter.getItemCount() == 0) {
            emptyRecyclerView.setVisibility(View.VISIBLE);
            recyclerViewStations.setVisibility(View.GONE);
        } else {
            emptyRecyclerView.setVisibility(View.GONE);
            recyclerViewStations.setVisibility(View.VISIBLE);
        }

    }



}
