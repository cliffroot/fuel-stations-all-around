package com.pwr.fuelstations.database;

/**
 * Created by cliffroot on 4/21/15.
 */
public class DatabaseColumns {

    public static final String STATION_TABLE_NAME   = "station_table_name";

    public static final String STATION_NAME         = "station_name";
    public static final String STATION_ADDRESS      = "station_address";
    public static final String STATION_PB95         = "station_pb95";
    public static final String STATION_PB98         = "station_pb98";
    public static final String STATION_ON           = "station_on";
    public static final String STATION_ONBIO        = "station_onbio";
    public static final String STATION_LPG          = "station_lpg";
    public static final String STATION_CNG          = "station_cng";
    public static final String STATION_LATITUDE     = "station_latitude";
    public static final String STATION_LONGITUDE    = "station_longitude";
    public static final String STATION_ID           = "station_id";



}
