package com.pwr.fuelstations.helper;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.pwr.fuelstations.AppApplication;
import com.pwr.fuelstations.database.StationDao;
import com.pwr.fuelstations.provider.RestStationProvider;
import com.pwr.fuelstations.provider.StationProvider;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EService;

import java.util.Calendar;

import java8.util.stream.StreamSupport;

/**
 * Created by cliffroot on 18.01.16.
 */
@EService
public class UpdateService extends IntentService {

    @Bean(RestStationProvider.class)
    StationProvider stationProvider;

    public UpdateService () {
        super("update_service");
    }

    public UpdateService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.e("UPDATE", "update_service_launched_yeah");
        synchronize();
    }

    @Background
    protected void synchronize () {
        stationProvider.getAllStations(stations -> {
            StationDao stationDao = ((AppApplication) getApplication()).stationDao;
            Log.e("UPDATE" , "there are " + stations.size() + " stations");
            new Thread(() ->  StreamSupport.stream(stations).forEach(stationDao::update)).start();
        });

        Utils.setLastUpdated(this, Calendar.getInstance().getTimeInMillis());

    }
}
