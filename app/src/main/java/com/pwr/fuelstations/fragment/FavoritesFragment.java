package com.pwr.fuelstations.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.pwr.fuelstations.R;
import com.pwr.fuelstations.adapter.StationAdapter;
import com.pwr.fuelstations.helper.Utils;
import com.pwr.fuelstations.provider.DatabaseStationProvider;
import com.pwr.fuelstations.provider.StationProvider;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

/**
 * Created by cliffroot on 22.11.15.
 */
@EFragment(R.layout.fragment_favorites)
public class FavoritesFragment extends Fragment {
    public final static String CURRENT_LOCATION_ARG = "current_location_arg";

    @ViewById(R.id.recycler_view_stations)
    RecyclerView stationsView;

    @ViewById(R.id.empty_recycler_view)
    TextView emptyRecyclerView;

    @ViewById(R.id.station_loading_progress_bar)
    ProgressBar loadingProgressBar;

    @Bean(DatabaseStationProvider.class)
    StationProvider stationProvider;

    @FragmentArg(CURRENT_LOCATION_ARG)
    LatLng currentLocation;

    static Fragment instance;

    public static Fragment getInstance (LatLng currentLocation) {
        if (instance == null) {
            instance = new FavoritesFragment_();
            Bundle bundle = new Bundle();
            bundle.putParcelable(CURRENT_LOCATION_ARG, currentLocation);
            instance.setArguments(bundle);
        }
        return instance;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupRecycler();
    }

    void setupRecycler () {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        stationsView.setLayoutManager(layoutManager);
        stationsView.setItemAnimator(new DefaultItemAnimator());

        stationsView.setVisibility(View.INVISIBLE);
        loadingProgressBar.setVisibility(View.VISIBLE);

        Utils.getFavoriteStations(getActivity(), stationProvider, stations -> {
            final StationAdapter adapter = new StationAdapter(getActivity(), stations, currentLocation, 0, false);
            if (getActivity() == null) return;
            getActivity().runOnUiThread(() -> {

                loadingProgressBar.setVisibility(View.INVISIBLE);
                stationsView.setAdapter(adapter);

                if (stationsView.getAdapter().getItemCount() == 0) {
                    emptyRecyclerView.setVisibility(View.VISIBLE);
                    stationsView.setVisibility(View.GONE);
                } else {
                    emptyRecyclerView.setVisibility(View.GONE);
                    stationsView.setVisibility(View.VISIBLE);
                }
            });
        });
    }
}
