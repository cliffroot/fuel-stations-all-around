package com.pwr.fuelstations;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;
import com.pwr.fuelstations.database.DatabaseHelper;
import com.pwr.fuelstations.database.StationDao;
import com.pwr.fuelstations.event.LocationRecievedEvent;
import com.pwr.fuelstations.helper.UpdateBroadcastReceiver;
import com.pwr.fuelstations.helper.Utils;
import com.pwr.fuelstations.rest.RestService;
import com.squareup.otto.Bus;

import java.util.Calendar;

import retrofit.RestAdapter;

/**
 * Created by cliffroot on 21.11.15.
 */
public class AppApplication extends Application implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{

    public Bus bus;
    private GoogleApiClient googleApiClient;
    private Location lastKnownLocation;

    public StationDao  stationDao;
    public RestService restService;

    @Override
    public void onCreate() {
        super.onCreate();
        bus = new Bus();
        buildGoogleApiClient();

        DatabaseHelper helper = new DatabaseHelper(this, "station", "stations.sql");
        stationDao = new StationDao(helper.getWritableDatabase());

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://52.29.75.17:4567")
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(msg -> Log.i("RETROFIT", msg))
                .build();

        restService = restAdapter.create(RestService.class);

        if (Calendar.getInstance().getTimeInMillis() - Utils.getLastUpdated(this)  < AlarmManager.INTERVAL_HOUR * 3000
                || Utils.getLastUpdated(this) == 0) {
            Intent intent = new Intent(this, UpdateBroadcastReceiver.class);
            intent.setAction(UpdateBroadcastReceiver.ACTION);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(System.currentTimeMillis());
            AlarmManager alarm = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
            alarm.cancel(pendingIntent);
            alarm.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
        }
    }


    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    public LatLng getCurrentLocation () {
        Location l = lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (l == null) {
            l = new Location("Poland");
            l.setLatitude(52.121072);
            l.setLongitude(19.167104);
        }
        return new LatLng(l.getLatitude(), l.getLongitude());
    }

    @Override
    public void onConnected(Bundle bundle) {
        lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (lastKnownLocation == null) {
            Log.e("LOCATION", "LOCATION IS NULL");
            lastKnownLocation = new Location("Poland");
            lastKnownLocation.setLatitude(52.121072);
            lastKnownLocation.setLongitude(19.167104);
        }
        Toast.makeText(this, "Unable to get your location. Please check your device settings", Toast.LENGTH_LONG).show();
        bus.post(new LocationRecievedEvent(lastKnownLocation));
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

}
