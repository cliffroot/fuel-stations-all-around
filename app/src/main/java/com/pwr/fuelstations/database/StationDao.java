package com.pwr.fuelstations.database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.pwr.fuelstations.model.StationPosition;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by cliffroot on 4/21/15.
 */
public class StationDao {
    SQLiteDatabase db;

    public StationDao(SQLiteDatabase database) {
        db = database;
    }

    public void insert(StationPosition station) {
        long id = db.insert(DatabaseColumns.STATION_TABLE_NAME, null, station.toContentValues());
        Log.e("ID_INSERTED", "id == " + id);
    }

    public void update(StationPosition station) {
        long id = db.update(DatabaseColumns.STATION_TABLE_NAME, station.toContentValues(), DatabaseColumns.STATION_ID + "=?",
                new String[]{String.valueOf(station.getId())});
        if (station.getId() % 100 == 0) {
            Log.e("ID_UPDATED", "id == " + station.getId());
        }
    }

    public StationPosition getById (long id) {
        Cursor cursor = db.query(DatabaseColumns.STATION_TABLE_NAME, null, DatabaseColumns.STATION_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null);
        if (cursor.moveToNext()) {
            return constructStation(cursor);
        }
        return null;
    }

    public List<StationPosition> getAll () {
        Log.e("GET_ALL", "stations_start");
        List<StationPosition> res = new LinkedList<>();
        Cursor cursor = db.query(DatabaseColumns.STATION_TABLE_NAME, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            res.add(constructStation(cursor));
        }
        Log.e("GET_ALL", "stations_end");
        return res;
    }

    private StationPosition constructStation (Cursor cursor) {
        StationPosition position = new StationPosition();
        position.setName(cursor.getString(0));
        position.setAddress(cursor.getString(1));
        position.setPb95(cursor.getString(2));
        position.setPb98(cursor.getString(3));
        position.setOn(cursor.getString(4));
        position.setOnbio(cursor.getString(5));
        position.setCNG(cursor.getString(6));
        position.setLPG(cursor.getString(7));
        position.setLatitude(cursor.getFloat(8));
        position.setLongitude(cursor.getFloat(9));
        position.setId(cursor.getLong(10));

        return position;

    }

}
