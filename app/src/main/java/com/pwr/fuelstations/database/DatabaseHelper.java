package com.pwr.fuelstations.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by cliffroot on 4/21/15.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private final static int DATABASE_VERSION = 1;
    private final static String DATABASE_NAME = "stations";

    private final static String CREATE_STATIONS_TABLE = "create table if not exists " +
            DatabaseColumns.STATION_TABLE_NAME + " (" +
            DatabaseColumns.STATION_NAME + " text not null, " +
            DatabaseColumns.STATION_ADDRESS + " text not null, " +
            DatabaseColumns.STATION_PB95 + " text, " +
            DatabaseColumns.STATION_PB98 + " text, " +
            DatabaseColumns.STATION_ON + " text, " +
            DatabaseColumns.STATION_ONBIO + " text, " +
            DatabaseColumns.STATION_CNG + " text, " +
            DatabaseColumns.STATION_LPG + " text, " +
            DatabaseColumns.STATION_LATITUDE + " float not null, " +
            DatabaseColumns.STATION_LONGITUDE + " float not null, " +
            DatabaseColumns.STATION_ID + " integer not null, " +
            "primary key (" + DatabaseColumns.STATION_NAME + ", " + DatabaseColumns.STATION_ADDRESS + ")" +
            ");";

    private Context context;
    private String assetPath;
    private String dbPath;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public DatabaseHelper(Context context, String dbName, String assetPath) {
        super(context, dbName, null, 1);
        this.context = context;
        this.assetPath = assetPath;
        this.dbPath = "/data/data/" + context.getApplicationContext().getPackageName() + "/databases/" + dbName;
        try {
            checkExists();
        } catch (IOException ex) {
            Log.e("DatabaseHelper", "Something went terribly terribly wrong", ex);
        }
    }

    private void checkExists() throws IOException {
        File dbFile = new File(dbPath);
        if (!dbFile.exists()) {
            dbFile.getParentFile().mkdirs();
            copyStream(context.getAssets().open(assetPath), new FileOutputStream(dbFile));
        }
    }

    private void copyStream(InputStream is, OutputStream os) throws IOException {
        byte buf[] = new byte[1024];
        int c;
        while (true) {
            c = is.read(buf);
            if (c == -1) {
                break;
            }
            os.write(buf, 0, c);
        }
        is.close();
        os.close();
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_STATIONS_TABLE);
        sqLiteDatabase.enableWriteAheadLogging();
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {

    }
}