package com.pwr.fuelstations.rest;


import com.pwr.fuelstations.model.StationPosition;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;


public interface RestService {

    @GET("/all")
    void retrieveStations(Callback<List<StationPosition>> callback);

    @POST("/update/{id}")
    void updatePrice (@Path("id") long id,  @Query("price_pb95") double pb95,
                      @Query("price_pb98") double pb98, @Query("price_on") double pbOn,
                      @Query("price_on_bio") double pbOnBio, @Query("price_cng") double cng,
                      @Query("price_lpg") double lpg,@Body Object dummy, Callback<Void> callback);
}
