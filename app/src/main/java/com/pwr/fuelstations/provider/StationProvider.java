package com.pwr.fuelstations.provider;

import com.google.android.gms.maps.model.LatLngBounds;
import com.pwr.fuelstations.model.StationPosition;

import java.util.List;

/**
 * Created by cliffroot on 21.11.15.
 */
public interface StationProvider {

    interface Callback {
        void onDone (List<StationPosition> stations);
    }

    void getAllStations (Callback callback);

    void getAllStationWithinBounds (LatLngBounds bounds, Callback callback);

}
