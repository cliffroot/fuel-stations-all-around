package com.pwr.fuelstations.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by cliffroot on 18.01.16.
 */
public class UpdateBroadcastReceiver extends BroadcastReceiver {

    public final static String ACTION = "com.pwr.fuelstations.UPDATE_ACTION";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ACTION)) {
            context.startService(new Intent(context, UpdateService_.class));
        }
    }
}
