package com.pwr.fuelstations.provider;

import android.content.Context;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.pwr.fuelstations.AppApplication;
import com.pwr.fuelstations.model.StationPosition;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by cliffroot on 21.11.15.
 */
@EBean
public class DummyStationProvider implements StationProvider{

    private static final String DIVIDER = ";";

    Context context;

    public DummyStationProvider (Context context) {
        this.context = context;
    }

    public List<StationPosition> stations = null;


    @Override
   // @Background
    public void getAllStations(Callback callback) {
        if (stations != null) {
            callback.onDone(stations);

            return;
        }

        stations = new LinkedList<>();
        InputStream fIn = null;
        InputStreamReader isr = null;
        BufferedReader input = null;
        try {
            fIn = context.getResources().getAssets().open("stacje_all.csv", Context.MODE_PRIVATE);
            isr = new InputStreamReader(fIn);
            input = new BufferedReader(isr);
            String line = "";
            while ((line = input.readLine()) != null) {
                try {
                    String[] tokens = line.split(DIVIDER);
                    StationPosition sp = new StationPosition();
                    sp.setName(tokens[0]);
                    sp.setAddress(tokens[1]);
                    sp.setPb95(tokens[2]);
                    sp.setPb98(tokens[3]);
                    sp.setOn(tokens[4]);
                    sp.setOnbio(tokens[5]);
                    sp.setLPG(tokens[6]);
                    sp.setCNG("   "); //TODO: clarify which one is missing from the .csv file
                    String latlng= tokens[7];

                    sp.setLatitude(Double.valueOf(latlng.split(",")[0].replace("\\s", "")));
                    sp.setLongitude(Double.valueOf(latlng.split(",")[1].replace("\\s", "")));
                    //sp.setLatitude(Double.valueOf(latlng.split(",")[0].split("Lat: ")[1]));
                    //sp.setLongitude(Double.valueOf(latlng.split(",")[1].split("lon: ")[1]));
                    stations.add(sp);

                    ((AppApplication) context.getApplicationContext()).stationDao.insert(sp);
                    Log.w("STATIONS", sp + " saved");
                } catch (Exception aiex) {
                    //Log.e("readFromAssets", "some fail", aiex);
                    // no coordinates, forget about this one
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            e.getMessage();
        } finally {
            try {
                if (isr != null)
                    isr.close();
                if (fIn != null)
                    fIn.close();
                if (input != null)
                    input.close();
            } catch (Exception e2) {
                e2.getMessage();
            }
        }
        callback.onDone(stations);
    }

    @Override
    @Background
    public void getAllStationWithinBounds(LatLngBounds bounds, Callback callback) {
        if (stations == null) {
            getAllStations(stations1 -> {
                List<StationPosition> result = new LinkedList<>();
                for (StationPosition station : stations1) {
                    if (bounds.contains(new LatLng(station.getLatitude(), station.getLongitude()))) {
                        result.add(station);
                    }
                }
                callback.onDone(result);
            });
        } else {
            Log.w("STATIONS", "LOOP THROUGH READY STATIONS");
            List<StationPosition> result = new LinkedList<>();
            for (int i = 0; i < stations.size(); i++) {
                StationPosition station = stations.get(i);
                if (station == null) {
                    continue;
                }
                if (bounds.contains(new LatLng(station.getLatitude(), station.getLongitude()))) {
                    result.add(station);
                }
            }
            callback.onDone(result);
        }
    }

}
