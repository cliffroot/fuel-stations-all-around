package com.pwr.fuelstations;

import android.os.Environment;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.pwr.fuelstations.fragment.FavoritesFragment;
import com.pwr.fuelstations.fragment.ListFragment;
import com.pwr.fuelstations.fragment.MapFragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.ViewById;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @ViewById(R.id.nav_view)
    NavigationView navigationView;

    @ViewById(R.id.fragment_container)
    FrameLayout fragmentContainer;

    @InstanceState
    int currentPosition = -1;

    int[] titles = new int[]{R.string.map_menu_nav_drawer, R.string.list_menu_nav_drawer, R.string.favorites_menu_nav_drawer};

    @AfterViews
    void setupViews () {
        setSupportActionBar(toolbar);

        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        if (currentPosition == -1) {
            navigationView.getMenu().performIdentifierAction(R.id.nav_map, 0);
        }
    }

    @Override
    public void onResume () {
        super.onResume();
        setTitle(titles[currentPosition]);

        copyDatabaseToVisibleLocation();
    }


    private void copyDatabaseToVisibleLocation () {
        try {
            File f = new File(getFilesDir().getParentFile().toString() + "/databases/stations");
            FileInputStream fis = new FileInputStream(f);
            byte [] buff = new byte[(int) f.length()];
            fis.read(buff);

            FileOutputStream fos = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/base.ololo");
            fos.write(buff);
            fos.close();
        } catch (IOException ioex) {
            ioex.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.nav_map) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, MapFragment.getInstance()).commit();
            currentPosition = 0;
        } else if (id == R.id.nav_list) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    ListFragment.getInstance(((AppApplication) getApplication()).getCurrentLocation() )).commit();
            currentPosition = 1;
        } else if (id == R.id.nav_favorites) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    FavoritesFragment.getInstance(((AppApplication) getApplication()).getCurrentLocation())).commit();
            currentPosition = 2;
        }

        setTitle(titles[currentPosition]);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
